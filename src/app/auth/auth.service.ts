import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { User } from './user.model';
import { Router } from '@angular/router';

export interface SignUpResponse {
  idToken: string;
  email: string;
  refreshToken: string;
  expiresIn: string;
  localId: string;
}

export interface LoginResponse {
  idToken: string;
  email: string;
  refreshToken: string;
  expiresIn: string;
  localId: string;
  registered: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userSubject: BehaviorSubject<User>;
  private logoutTimer: any;
  private apiToken = 'AIzaSyBgCPUbmn_E5Salrd8jDzFy0Z1Bju4kFVQ';
  private baseUrl = 'https://identitytoolkit.googleapis.com/v1/accounts';
  private loginActionSuffix = ':signInWithPassword?key=' + this.apiToken;
  private signupActionSuffix = ':signUp?key=' + this.apiToken;

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    this.userSubject = new BehaviorSubject<User>(null);
    this.autoLogin();
  }

  login(email: string, password: string): Observable<LoginResponse> {
    return this.http.post<LoginResponse>(
      this.baseUrl + this.loginActionSuffix, {
        email,
        password,
        returnSecureToken: true
      }
    ).pipe(
      tap(response => this.handleAuthentication(response)),
    );
  }

  autoLogin() {
    const userData: {
      email: string,
      id: string,
      _token: string,
      _tokenExpiryDate: string
    } =  JSON.parse(localStorage.getItem('userData'));
    if (userData) {
      const user = new User(
        userData.email,
        userData.id,
        userData._token,
        new Date(userData._tokenExpiryDate)
      );
      if (user.token) {
        this.userSubject.next(user);
        this.autoLogout(
          new Date(userData._tokenExpiryDate).getTime() - new Date().getTime()
        );
      }
    }
  }

  autoLogout(expiryMs: number) {
    if (!this.logoutTimer) {
      this.logoutTimer = setTimeout(() => {
        this.logout();
      }, expiryMs);
    }
  }

  logout() {
    this.userSubject.next(null);
    localStorage.removeItem('userData');
    this.router.navigate(['/auth']);
    if (this.logoutTimer) {
      clearTimeout(this.logoutTimer);
      this.logoutTimer = null;
    }
  }

  signup(email: string, password: string): Observable<SignUpResponse> {
    return this.http.post<SignUpResponse>(
      this.baseUrl + this.signupActionSuffix, {
        email,
        password
      }
    ).pipe(
      tap(response => this.handleAuthentication(response))
    );
  }

  handleAuthentication(response: any) {
    const expirationDate = new Date(new Date().getTime() + (+response.expiresIn * 1000));
    const user = new User(
      response.email,
      response.localId,
      response.idToken,
      expirationDate
    );
    localStorage.setItem('userData', JSON.stringify(user));
    this.autoLogout((+response.expiresIn * 1000));
    this.userSubject.next(user);
  }
}
