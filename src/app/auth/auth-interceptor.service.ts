import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpParams
} from '@angular/common/http';
import { AuthService } from './auth.service';
import { take, exhaustMap } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private authService: AuthService
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    return this.authService.userSubject.pipe(
      // Take the last emit(), possible with BehaviorSubject
      take(1),
      // Waits for the first one to be unsubscribed and execute
      // Allows changes in the output value, this time to Recipe[]
      exhaustMap(
        user => {
          // The first non-authenticated request should have a null user.
          if (user) {
            const interceptedReq = req.clone({params: new HttpParams().set(
              'auth', user.token
            )});
            return next.handle(interceptedReq);
          }
          return next.handle(req);
        }
      )
    );
  }
}
