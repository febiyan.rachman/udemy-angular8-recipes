import { Component, OnInit, ComponentFactoryResolver, ViewChild, ViewContainerRef, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { AlertComponent } from '../shared/alert/alert.component';
import { PlaceholderDirective } from '../shared/placeholder.directive';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
// tslint:disable:object-literal-key-quotes
export class AuthComponent implements OnInit, OnDestroy {

  isLoginMode = true;
  isError = false;
  isLoading = false;
  message = null;
  // To unsubscribe on destroy
  private alertSubscription: Subscription;
  // Instead creating with a selector, we use the type, so that we can exactly get the Type in return
  @ViewChild(PlaceholderDirective, { static: false }) alertPlaceholder: PlaceholderDirective;

  constructor(
    private authService: AuthService,
    private router: Router,
    private factory: ComponentFactoryResolver
    ) { }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    if (this.alertSubscription) {
      this.alertSubscription.unsubscribe();
    }
  }

  onSubmit(form: NgForm) {
    this.isLoading = true;
    if (this.isLoginMode) {
      this.authService.login(form.value.email, form.value.password).subscribe(
        response => {
          this.router.navigate(['/recipes']);
          this.isLoading = false;
        },
        (errorResponse: HttpErrorResponse) => {
          this.isLoading = false;
          this.showMessage(errorResponse.error.error.message);
        }
      );
    } else {
      this.authService.signup(form.value.email, form.value.password).subscribe(
        response => {
          this.isLoginMode = true;
          this.message = 'Signed up!';
          this.isLoading = false;
        },
        (errorResponse: HttpErrorResponse) => {
          this.isLoading = false;
          this.showMessage(errorResponse.error.error.message);
        }
      );
    }
    form.reset();
  }

  onSwitch() {
    this.isLoginMode = !this.isLoginMode;
  }

  showMessage(errorMessage: string) {
    // Clear the placeholder
    const placeholderViewContainerRef: ViewContainerRef = this.alertPlaceholder.viewContainerRef;
    placeholderViewContainerRef.clear();
    // Create an instance of the  alert component and get the reference
    // Dont create component manually, use a factory resolver
    const alertComponentFactory = this.factory.resolveComponentFactory(AlertComponent);
    const alertComponentRef = placeholderViewContainerRef.createComponent(alertComponentFactory);
    // Access the instance its properties
    alertComponentRef.instance.message = 'Error occured: ' + errorMessage;
    this.alertSubscription = alertComponentRef.instance.close.subscribe(() => {
      this.alertSubscription.unsubscribe();
      placeholderViewContainerRef.clear();
    });
  }

  dismissMessage() {
    this.message = null;
  }

  get messageClass() {
    return {
      'alert': this.isError,
      'alert-danger': this.isError
    };
  }
}
