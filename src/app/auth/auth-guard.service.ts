import { Injectable } from '@angular/core';
import {
  CanActivate,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  UrlTree,
  Router
} from '@angular/router';
import {
  Observable
} from 'rxjs';
import { AuthService } from './auth.service';
import { map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  constructor(private authService: AuthService, private router: Router) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | UrlTree | Observable<boolean|UrlTree> | Promise<boolean|UrlTree> {
    return this.authService.userSubject.pipe(
      take(1),
      map(user => {
        // If user is null, it will be true-ish because of the first !
        // The second ! will negate it to false
        const isAuthenticated = !!user;
        if (isAuthenticated) {
          return isAuthenticated;
        }
        // Redirect without router.navigate. New in angular
        return this.router.createUrlTree(['/auth']);
      })
    );
  }

}
