import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShoppingListRoutingModule } from './shopping-list-routing.module';
import { ShoppingListComponent } from './shopping-list.component';
import { ShoppingListEditorComponent } from './shopping-list-editor/shopping-list-editor.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    ShoppingListComponent,
    ShoppingListEditorComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ShoppingListRoutingModule
  ]
})
export class ShoppingListModule { }
