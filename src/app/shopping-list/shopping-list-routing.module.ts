import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShoppingListComponent } from './shopping-list.component';
import { ShoppingListEditorComponent } from './shopping-list-editor/shopping-list-editor.component';
import { AuthGuardService } from '../auth/auth-guard.service';

const routes: Routes = [
  {
      path: '',
      component: ShoppingListComponent,
      canActivate: [AuthGuardService],
      children: [
          { path: '', component: ShoppingListEditorComponent },
          { path: ':id/edit', component: ShoppingListEditorComponent }
      ]
  }
];


@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ShoppingListRoutingModule { }
