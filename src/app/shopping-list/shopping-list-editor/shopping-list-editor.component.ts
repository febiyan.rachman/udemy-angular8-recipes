import { Component, OnInit, ViewChild } from '@angular/core';
import { Ingredient } from 'src/app/shopping-list/ingredient.model';
import { ShoppingService } from '../shopping.service';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-shopping-list-editor',
  templateUrl: './shopping-list-editor.component.html',
  styleUrls: ['./shopping-list-editor.component.css']
})
export class ShoppingListEditorComponent implements OnInit {
  editMode = false;
  ingredient: Ingredient;
  ingredientIndex: number;
  @ViewChild('ingredientForm', { static: false }) ingredientForm: NgForm;

  constructor(
    private shoppingService: ShoppingService,
    private route: ActivatedRoute,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      if (params['id']) {
        this.ingredientIndex = +params['id'];
        this.editMode = true;
        this.ingredient = this.shoppingService.getIngredient(+this.ingredientIndex);
        // There is an error if I dont use setTimeout
        setTimeout(() => {
          this.ingredientForm.setValue({
            name: this.ingredient.name,
            amount: this.ingredient.amount
          });
        });
      }
    });
  }

  onFormSubmit() {
    if (!this.editMode) {
      this.shoppingService.addIngredient(
        new Ingredient(this.ingredientForm.value.name, this.ingredientForm.value.amount)
      );
    } else {
      this.shoppingService.updateIngredient(
        new Ingredient(this.ingredientForm.value.name, this.ingredientForm.value.amount),
        this.ingredientIndex
      );
    }
    this.router.navigate(['/shopping-list']);
  }

  onClear() {
    this.ingredientForm.reset();
  }

  onDelete() {
    this.shoppingService.deleteIngredient(this.ingredientIndex);
    this.router.navigate(['/shopping-list']);
  }
}
