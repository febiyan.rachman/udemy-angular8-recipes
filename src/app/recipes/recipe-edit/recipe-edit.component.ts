import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Recipe } from '../recipe.model';
import { RecipeService } from '../recipe.service';
import { NgForm, FormGroup, FormControl, Validators, FormArray, Form } from '@angular/forms';
import { TranslationWidth } from '@angular/common';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {
  // tslint:disable:object-literal-key-quotes
  // tslint:disable:no-string-literal
  editMode: boolean;
  recipeId: number;
  recipe: Recipe;
  recipeForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private recipeService: RecipeService,
    private router: Router
  ) {
    this.editMode = false;
    this.recipe = new Recipe(null, null, null, null, []);
    this.recipeForm = new FormGroup({
      'name': new FormControl(this.recipe.name, [Validators.required]),
      'imagePath': new FormControl(this.recipe.imagePath),
      'description': new FormControl(this.recipe.description, [Validators.required]),
      'ingredients': new FormArray([])
    });
  }

  ngOnInit() {
    this.route.params.subscribe(
      (params: Params) => {
        this.recipeId = +params['id'];
        if (!isNaN(this.recipeId)) {
          this.editMode = true;
          this.recipe = this.recipeService.getRecipeById(this.recipeId);
        }
        this.recipeForm.patchValue({
          'name': this.recipe.name,
          'imagePath': this.recipe.imagePath,
          'description': this.recipe.description
        });
        for (const ingredient of this.recipe.ingredients) {
          this.ingredientControls.push(
            new FormGroup({
              'name': new FormControl(ingredient.name, [Validators.required]),
              'amount': new FormControl(ingredient.amount, [Validators.required, Validators.pattern(/^[1-9]+[0-9]*$/)])
            })
          );
        }
        console.log(this.ingredientControls);
      }
    );
  }

  onRecipeSave() {
    const recipe: Recipe = this.recipeForm.value as Recipe;
    if (this.editMode) {
      // Update
      recipe.id = this.recipeId;
      this.recipeService.updateRecipe(this.recipeId, recipe);
    } else {
      // Save
      this.recipeService.addRecipe(recipe);
    }
  }

  onNewIngredient() {
    this.ingredientControls.push(
      new FormGroup({
        'name': new FormControl(null, [Validators.required]),
        'amount': new FormControl(null, [Validators.required, Validators.pattern('^[0-9]+$')])
      })
    );
  }

  onDeleteIngredient(index: number) {
    this.ingredientControls.removeAt(index);
  }

  onCancel() {
    this.router.navigate(['/recipes']);
  }

  get ingredientControls() {
    return (this.recipeForm.get('ingredients') as FormArray);
  }
}
