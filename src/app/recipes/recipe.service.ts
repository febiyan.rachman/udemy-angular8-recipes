import { Injectable } from '@angular/core';
import { Recipe } from './recipe.model';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  public recipesChanged: Subject<Recipe[]>;
  private recipes: Recipe[];

  constructor() {
    this.recipesChanged = new Subject<Recipe[]>();
    this.recipes = [];
  }

  getRecipes() {
    return this.recipes.slice();
  }

  getRecipeById(id: number): Recipe {
    return this.recipes.find(recipe => recipe.id === id);
  }

  updateRecipe(id: number, recipe: Recipe) {
    const index = this.recipes.indexOf(this.recipes.find(x => x.id === id));
    this.recipes[index] = recipe;
    this.recipesChanged.next(this.recipes.slice());
  }

  addRecipe(recipe: Recipe) {
    const biggestId = Math.max(...this.recipes.map(x => x.id));
    recipe.id = biggestId + 1;
    this.recipes.push(recipe);
    this.recipesChanged.next(this.recipes.slice());
  }

  deleteRecipe(id: number) {
    const index = this.recipes.indexOf(this.recipes.find(x => x.id === id));
    this.recipes.splice(index, 1);
    this.recipesChanged.next(this.recipes.slice());
  }

  setRecipes(recipes: Recipe[]) {
    this.recipes = recipes;
    this.recipesChanged.next(this.recipes.slice());
  }
}
