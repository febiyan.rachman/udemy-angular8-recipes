import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Recipe } from '../recipes/recipe.model';
import { RecipeService } from '../recipes/recipe.service';
import { Observable } from 'rxjs';
import { map, tap, take, exhaustMap } from 'rxjs/operators';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class DataStorageService {
  private url = 'https://udemy-angular8-recipe.firebaseio.com/recipes.json';
  constructor(
    private http: HttpClient,
    private recipeService: RecipeService,
    private authService: AuthService
    ) { }

  storeRecipes() {
    const recipes = this.recipeService.getRecipes();
    if (recipes.length === 0) {
      return;
    }
    this.http.put(
      this.url,
      this.recipeService.getRecipes()
    ).subscribe((response) => {
      console.log(response);
    });
  }

  fetchRecipes(): Observable<Recipe[]> {
    return this.http.get<Recipe[]>(this.url)
    .pipe(
      // Map every response
      map((recipes) => {
        console.log('mapped');
        // Map every element in the array
        // Make sure that every recipe has ingredients
        return recipes.map(recipe => {
          return {
            ...recipe,
            ingredients: recipe.ingredients ? recipe.ingredients : []
          };
        });
      }),
      tap((recipes) => {
        // Tap the recipes to the service
        console.log('tapped');
        this.recipeService.setRecipes(recipes);
      })
    );
  }
}
