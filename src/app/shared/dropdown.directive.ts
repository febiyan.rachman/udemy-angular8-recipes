import {
    Directive,
    ElementRef,
    HostListener,
    HostBinding
} from '@angular/core';
@Directive({
    selector: '[appDropdown]'
})
export class DropdownDirective {
    @HostBinding('class.open') isOpen = false;
    @HostListener('document:click', ['$event']) toggleDropdown(event: Event) {
        this.isOpen = this.elRef.nativeElement.contains(event.target);
    }
    constructor(private elRef: ElementRef) {

    }
}
