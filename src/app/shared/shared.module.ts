import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerComponent } from './spinner/spinner.component';
import { AlertComponent } from './alert/alert.component';
import { PlaceholderDirective } from './placeholder.directive';
import { DropdownDirective } from './dropdown.directive';


@NgModule({
  declarations: [
    SpinnerComponent,
    AlertComponent,
    PlaceholderDirective,
    DropdownDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    SpinnerComponent,
    AlertComponent,
    PlaceholderDirective,
    DropdownDirective
  ],
  // Components that are eventually created without selectors
  entryComponents: [AlertComponent]
})
export class SharedModule { }
